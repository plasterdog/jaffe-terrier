<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package plasterdog_progressive_flexbox
 */

get_header(); ?>
	<div id="content" class="site-content">
	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
		<?php
		while ( have_posts() ) : the_post(); ?>

		
		<div class="advice-header">
		<h1 class="entry-title"><?php the_title();?></h1>	
		<div class="advice-query">
		<?php the_field('advice_question'); ?>
		<h4><?php the_field('advice_seeker'); ?></h4>
		</div><!-- ends advice query -->
		</div><!-- ends advice header-->
		
			<div class="advice-meta-section">
			<?php echo get_avatar($post->post_author, '300', $avatar); ?>	<br />	
			<div class="advice-meta-title-section">						
			<?php the_author_meta('nickname'); ?><br />
			<?php the_time('l, F j, Y') ?>
			</div><!-- ends advice meta title section -->					
			</div><!-- ends advice-meta-section-->
	
		<div class="advice-content-section">
		
<?php
$today = date('r');
$articledate = get_the_time('r');
$difference = round((strtotime($today) - strtotime($articledate))/(24*60*60),0);
if ($difference >= 400) { ?>

<h2 style="font-weight:600;">What’s Budleigh’s advice? Read it in Sleeping between Giants, Book II: Ask a Terrier.<br/>
Coming soon!
 &nbsp; <a href="/publication/budleigh-the-early-year/">Buy it now</a></h2>

<?php } ?>

<?php
$today = date('r');
$articledate = get_the_time('r');
$difference = round((strtotime($today) - strtotime($articledate))/(24*60*60),0);
if ($difference <= 400) { ?>

		<?php the_content(); ?>

			<?php 
			// If comments are open or we have at least one comment, load up the comment template.
			if ( comments_open() || get_comments_number() ) :
				comments_template();
			endif;
			
		?>
<?php } ?>
	
		<footer class="entry-footer"></footer><!-- .entry-footer -->
		<?php endwhile; ?> <!-- ends the loop -->
		</div><!-- ends advice-content-section-->

		</main><!-- #main -->
	</div><!-- #primary -->
<?php
get_sidebar();
get_footer();
