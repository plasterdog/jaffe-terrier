<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package plasterdog_progressive_flexbox
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">

<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
<link rel="stylesheet" href="<?php bloginfo('stylesheet_directory'); ?>/font-awesome/css/font-awesome.min.css">
<!--[if IE 8]>
<link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/ie8.css" media="screen" type="text/css" />
<![endif]-->
<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
<div id="page" class="site">	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'pdog-flex' ); ?></a>
	<header id="masthead" class="site-header" role="banner">
		<div class="masthead-holder">
					<div class="masthead-logo">
					
					</div><!-- ends masthead logo -->
					<div class="masthead-navigation">	
				<div class="top-masthead-holder">
		<div class="top-logo-side">
<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a>
	</div><!-- ends logo side -->
	<div class="top-contact-side">
			<div class="social">
				<ul class="top-social-icons">
					<?php if(get_option('pdog_phone') && get_option('pdog_phone') != '') {?>
					<li><span class="icon-text"><?php echo get_option('pdog_phone') ?></span> | </li><?php } ?>

					<?php if(get_option('pdog_address') && get_option('pdog_address') != '') {?>
					<li><span class="icon-text"><?php echo get_option('pdog_address') ?></span> | </li><?php } ?>

					<?php if(get_option('pdog_facebook') && get_option('pdog_facebook') != '') {?>
					<li><a href="<?php echo get_option('pdog_facebook') ?>" target="_blank"><i class="fa fa-facebook"></i></a>	</li><?php } ?>

					<?php if(get_option('pdog_linkedin') && get_option('pdog_linkedin') != '') {?>
					<li><a href="<?php echo get_option('pdog_linkedin') ?>" target="_blank"><i class="fa fa-linkedin"></i></a>	</li><?php } ?>	
						
					<?php if(get_option('pdog_googleplus') && get_option('pdog_googleplus') != '') {?>
					<li><a href="<?php echo get_option('pdog_googleplus') ?>" target="_blank"><i class="fa fa-google-plus"></i></a>	</li><?php } ?>

					<?php if(get_option('pdog_twitter') && get_option('pdog_twitter') != '') {?>
					<li><a href="<?php echo get_option('pdog_twitter') ?>" target="_blank"><i class="fa fa-twitter"></i></a>	</li><?php } ?>	

					<?php if(get_option('pdog_instagram') && get_option('pdog_instagram') != '') {?>
					<li><a href="<?php echo get_option('pdog_instagram') ?>" target="_blank"><i class="fa fa-instagram"></i></a>	</li><?php } ?>	
					
					<?php if(get_option('pdog_youtube') && get_option('pdog_youtube') != '') {?>
					<li><a href="<?php echo get_option('pdog_youtube') ?>" target="_blank"><i class="fa fa-youtube-square"></i></a>	</li><?php } ?>	

					<?php if(get_option('pdog_vimeo') && get_option('pdog_vimeo') != '') {?>
					<li><a href="<?php echo get_option('pdog_vimeo') ?>" target="_blank"><i class="fa fa-vimeo-square"></i></a>	</li><?php } ?>	

					<?php if(get_option('pdog_email') && get_option('pdog_email') != '') {?>
					<li><a href="mailto:<?php echo get_option('pdog_email') ?>" target="_blank"><i class="fa fa-envelope-o"></i></a>	</li><?php } ?>	
				</ul>
			</div><!-- ends social -->
		</div><!-- ends top contact side -->
		</div><!-- ends top masthead holder -->	
			<nav id="site-navigation" class="main-navigation" role="navigation">
					<button class="menu-toggle" aria-controls="primary-menu" aria-expanded="false">
					<i class="fa fa-bars"></i> </button>
					<div class="top-nav-bar">
					<?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_id' => 'primary-menu' ) ); ?>
					</div><!-- ends navbar -->			
			</nav><!-- #site-navigation -->	
		</div><!-- ends first masthead holder -->
	</header><!-- #masthead -->

