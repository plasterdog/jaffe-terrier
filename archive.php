<?php
/**
 * The template for displaying archive pages.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package plasterdog_progressive_flexbox
 */

get_header(); ?>

	<div id="content" class="site-content">
	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
		<?php
		if ( have_posts() ) : ?>
			<header class="page-header">
			<h1 class="entry-title"><?php single_cat_title( ); ?></h1>
				<?php the_archive_description(  ); ?>
			</header><!-- .page-header -->
			<hr/>
			<?php
			/* Start the Loop */
			while ( have_posts() ) : the_post(); ?>
					<?php if (!empty($post->post_excerpt)) : ?>	
					<div class="archive-thumb"><a href="<?php the_permalink(); ?>" rel="bookmark"><img src="<?php echo my_image_display(); ?>"/></a></div> 
					<div class="archive-excerpt"><h1 class="entry-title"><a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a></h1>
					<?php the_excerpt();?>
					<p class="archive-link"><a href="<?php the_permalink(); ?>" rel="bookmark">read more</a></p>
					</div><!-- ends archive excerpt -->
					<div class="clear"><hr/></div>
					<?php else : ?>
					<div class="content-area">
					<h1 class="entry-title"><?php the_title(); ?></a></h1>
					<?php the_content(); ?>
					</div>
					<div class="clear"><hr/></div>
					<?php endif; ?>
			<?php endwhile;

			the_posts_navigation();

		else :

			get_template_part( 'template-parts/content', 'none' );

		endif; ?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_sidebar();
get_footer();
