<?php
/**
 * The template for displaying archive pages.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package plasterdog_progressive_flexbox
 */

get_header(); ?>

	<div id="content" class="site-content">
	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
		<?php
		if ( have_posts() ) : ?>
			<header class="page-header" style="padding-top:1em;">
			<h1 class="entry-title"><?php single_cat_title( ); ?></h1>
				<?php the_archive_description(  ); ?>
			</header><!-- .page-header -->
			<hr/>
			<?php
			/* Start the Loop */
			while ( have_posts() ) : the_post(); ?>
<div class="clear">					
<div class="product-container">
		<div class="page_left_side"><a href="<?php the_permalink(); ?>" rel="bookmark"><img src="<?php the_field('publication_image'); ?>"></a></div>
		<div class="page_right_side">
<h1 class="entry-title"><a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title();?></a></h1>
<?php if ( get_field( 'publication_pricing_info' ) ): ?>	 
<?php the_field('publication_pricing_info'); ?>
<?php endif; // end of if field_name logic ?>
<?php
                    // check if the repeater field has rows of data
                    if( have_rows('links_to_vendors') ): ?>     
                    <ul>             
                    <?php while ( have_rows('links_to_vendors') ) : the_row(); ?>
<li>
<?php if( get_sub_field('publication_vender_link_label') ): ?>
<a href="<?php the_sub_field('publication_vender_link_target'); ?>" target="_blank">
<?php the_sub_field('publication_vender_link_label'); ?></a>
<?php endif; ?>
</li>
            <?php
	                 endwhile; ?>
	             </ul>
	                 <?php else : ?>
	                <?php  // no rows found
	                 endif; ?> 
<?php if ( get_field( 'publication_paypal_text' ) ): ?>	                 	
<h3><?php the_field('publication_paypal_text'); ?></h3>
<?php endif; // end of if field_name logic ?>

<?php if ( get_field( 'publication_paypal_button_code' ) ): ?>	   
<?php the_field('publication_paypal_button_code'); ?>
<?php endif; // end of if field_name logic ?>

<p class="product-link"><a href="<?php the_permalink(); ?>" rel="bookmark">See the full description</a></p>
</div><!-- ends product container -->

</div><!-- ends outer clear -->
<div class="clear"><hr/></div>

					

			<?php endwhile;

			the_posts_navigation();

		else :

			get_template_part( 'template-parts/content', 'none' );

		endif; ?>

		</main><!-- #main -->
	</div><!-- #primary -->
<?php if ( ! is_active_sidebar( 'sidebar-1' ) ) {
	return;
}
?>

<aside id="secondary" class="widget-area" role="complementary">

	<?php dynamic_sidebar( 'sidebar-2' ); ?>



</aside><!-- #secondary -->
<?php
get_footer();
