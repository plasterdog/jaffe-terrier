<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package plasterdog_progressive_flexbox
 */

?>

	</div><!-- #content -->
	
	<footer id="colophon" class="site-footer" role="contentinfo">


		<div class="site-info"> &copy;Copyright <?php $the_year = date("Y"); echo $the_year; ?> <?php bloginfo( 'name' ); ?> | <?php bloginfo('description'); ?>&nbsp; | All Rights Reserved.</div><!-- .site-info -->
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
