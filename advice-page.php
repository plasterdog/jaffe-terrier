<?php
/**
 * Template Name: Advice Page
 *
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package plasterdog_progressive_flexbox
 */

get_header(); ?>
<div id="content" class="site-content">
	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
			<div class="entry-content">
			<?php while ( have_posts() ) : the_post(); ?>


			<h1 class="entry-title"><?php the_title();?></h1>




			<?php the_content();?>

			<?php endwhile; // End of the loop.
			?>



			<hr/>
<?php 
    query_posts(array( 
        'post_type' => 'advice_post_type',
        'showposts' => -1 ,
        'orderby'=> 'date', 'order' => 'DESC'
    ) );  
?>

			<?php
			/* Start the Loop */
			while ( have_posts() ) : the_post(); ?>
					<?php if (!empty($post->post_excerpt)) : ?>	
						<div class="advice-header">
		<h1 class="entry-title"><?php the_title();?></h1>	
		<div class="advice-query">
		<?php the_field('advice_question'); ?>
		<h4><?php the_field('advice_seeker'); ?></h4>
		</div><!-- ends advice query -->
		</div><!-- ends advice header-->
					<div class="advice-meta-section"><a href="<?php the_permalink(); ?>" rel="bookmark">
						<?php echo get_avatar($post->post_author, '300', $avatar); ?>
					</a>
					<div class="advice-meta-title-section">									
					<?php the_author_meta('nickname'); ?><br />
					<?php the_time('l, F j, Y') ?>
					</div><!-- ends advice meta title section -->						
					</div> 
					<div class="advice-content-section">
					<?php the_excerpt();?>
					<p class="archive-link"><a href="<?php the_permalink(); ?>" rel="bookmark">read more</a></p>
					</div><!-- ends archive excerpt -->
					<div class="clear"><hr/></div>

					<?php else : ?>
		<div class="advice-header">
		<h1 class="entry-title"><?php the_title();?></h1>	
		<div class="advice-query">
		<?php the_field('advice_question'); ?>
		<h4><?php the_field('advice_seeker'); ?></h4>
		</div><!-- ends advice query -->
		</div><!-- ends advice header-->
					<div class="advice-meta-section"><a href="<?php the_permalink(); ?>" rel="bookmark">
					<?php echo get_avatar($post->post_author, '300', $avatar); ?>
					</a>
					<div class="advice-meta-title-section">						
					<?php the_author_meta('nickname'); ?><br />
					<?php the_time('l, F j, Y') ?>
					</div><!-- ends advice meta title section -->						
					</div> 
					<div class="advice-content-section">
					<?php the_content(); ?>
					</div>
					<div class="clear"><hr/></div>
					<?php endif; ?>
			<?php endwhile; ?>




		</div><!-- ends entry content -->
		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_sidebar();
get_footer();
