<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package plasterdog_progressive_flexbox
 */

get_header(); ?>
<div id="content" class="site-content">
	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
			<div class="entry-content">
			<?php while ( have_posts() ) : the_post(); ?>


			<h1 class="entry-title"><?php the_title();?></h1>




			<?php the_content();?>

			<?php endwhile; // End of the loop.
			?>
		</div><!-- ends entry content -->
		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_sidebar();
get_footer();
