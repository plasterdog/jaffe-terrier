<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package plasterdog_progressive_flexbox
 */

get_header(); ?>
	<div id="content" class="site-content">
	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
		<?php
		while ( have_posts() ) : the_post(); ?>

		<h1 class="entry-title"><?php the_title();?></h1>
		<div class="entry-meta">
		<?php the_time('l, F j, Y') ?><br />
		- <?php the_author_meta('nickname'); ?><br />
		</div><!-- .entry-meta -->

		

<?php
$today = date('r');
$articledate = get_the_time('r');
$difference = round((strtotime($today) - strtotime($articledate))/(24*60*60),0);
if ($difference >= 400) { ?>

		
<?php if (!empty($post->post_excerpt)) : ?>
	<br/>
		<?php
		the_excerpt( sprintf(
		/* translators: %s: Name of current post. */
		wp_kses( __( 'Continue reading %s <span class="meta-nav">&rarr;</span>', 'pdog-flex' ), array( 'span' => array( 'class' => array() ) ) ),
		the_title( '<span class="screen-reader-text">"', '"</span>', false )
		) );?>
	
<?php endif; ?>
<h2 style="font-weight:600;">Like what you’re reading? There’s more in Sleeping between Giants, Book I: Budleigh, the Early Year. &nbsp;
	<a href="/publication/budleigh-the-early-year/">Buy it now</a></h2>

<?php } ?>
<?php
$today = date('r');
$articledate = get_the_time('r');
$difference = round((strtotime($today) - strtotime($articledate))/(24*60*60),0);
if ($difference <= 400) { ?>
		<?php
		the_content( sprintf(
		/* translators: %s: Name of current post. */
		wp_kses( __( 'Continue reading %s <span class="meta-nav">&rarr;</span>', 'pdog-flex' ), array( 'span' => array( 'class' => array() ) ) ),
		the_title( '<span class="screen-reader-text">"', '"</span>', false )
		) );?>
		<footer class="entry-footer">
			<div class="page_left_side"><?php previous_post_link(); ?></div>
			<div class="page_right_side" style="text-align:right;"><?php next_post_link(); ?></div>
			</footer><!-- .entry-footer -->
			<?php 
			// If comments are open or we have at least one comment, load up the comment template.
			if ( comments_open() || get_comments_number() ) :
				comments_template();
			endif;
			// End of the loop.
		?>


<?php } ?>
	<?php endwhile;  ?>		

		</main><!-- #main -->
	</div><!-- #primary -->
<?php
get_sidebar();
get_footer();
