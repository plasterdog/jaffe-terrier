<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package plasterdog_progressive_flexbox
 */

get_header(); ?>
	<div id="content" class="site-content">
	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
		<?php
		while ( have_posts() ) : the_post(); ?>
<div class="product-container">
		<div class="page_left_side top-gap"><img src="<?php the_field('publication_image'); ?>"></div>
		<div class="page_right_side top-gap">
<h1 class="entry-title"><?php the_title();?></h1>
<?php if ( get_field( 'publication_pricing_info' ) ): ?>	 
<?php the_field('publication_pricing_info'); ?>
<?php endif; // end of if field_name logic ?>
<?php
                    // check if the repeater field has rows of data
                    if( have_rows('links_to_vendors') ): ?>     
                    <ul>             
                    <?php while ( have_rows('links_to_vendors') ) : the_row(); ?>
<li>
<?php if( get_sub_field('publication_vender_link_label') ): ?>
<a href="<?php the_sub_field('publication_vender_link_target'); ?>" target="_blank">
<?php the_sub_field('publication_vender_link_label'); ?></a>
<?php endif; ?>
</li>
            <?php
	                 endwhile; ?>
	             </ul>
	                 <?php else : ?>
	                <?php  // no rows found
	                 endif; ?> 
<?php if ( get_field( 'publication_paypal_text' ) ): ?>	                 	
<h3><?php the_field('publication_paypal_text'); ?></h3>
<?php endif; // end of if field_name logic ?>

<?php if ( get_field( 'publication_paypal_button_code' ) ): ?>	   
<?php the_field('publication_paypal_button_code'); ?>
<?php endif; // end of if field_name logic ?>

		</div>
<div class="description-container">
<?php if ( get_field( 'publication_description_heading' ) ): ?>	  
	<h2><?php the_field('publication_description_heading'); ?></h2>
<?php endif; // end of if field_name logic ?>	

	<?php the_content( );?>
		
	</div>


			<footer class="entry-footer">
			<div class="page_left_side"><?php previous_post_link(); ?></div>
			<div class="page_right_side" style="text-align:right;"><?php next_post_link(); ?></div>
			</footer><!-- .entry-footer -->
		</div><!-- ends product container -->
			<?php 
			// If comments are open or we have at least one comment, load up the comment template.
			if ( comments_open() || get_comments_number() ) :
				comments_template();
			endif;
			endwhile; // End of the loop.
		?>
		</main><!-- #main -->
	</div><!-- #primary -->

<?php if ( ! is_active_sidebar( 'sidebar-1' ) ) {
	return;
}
?>

<aside id="secondary" class="widget-area" role="complementary">

	<?php dynamic_sidebar( 'sidebar-2' ); ?>



</aside><!-- #secondary -->
<?php
get_footer();
