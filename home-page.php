<?php
/**
 * Template Name: Home Page
 *
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package plasterdog_progressive_flexbox
 */

get_header(); ?>
<div class="hero-logo">

	<div class="home-focus-section">

		<?php if( get_field('home_focus_link') ): ?>
		<a href="<?php the_field('home_focus_link'); ?>">		
		<img src="<?php the_field('home_focus_image'); ?>"></a>
		<?php endif; ?>	

		<?php if( !get_field('home_focus_link') ): ?>
		<img src="<?php the_field('home_focus_image'); ?>">
		<?php endif; ?>	


		<?php if( get_field('home_focus_call_to_action') ): ?>

			<?php if( get_field('home_focus_link') ): ?>
			<a href="<?php the_field('home_focus_link'); ?>"><h3><?php the_field('home_focus_call_to_action'); ?></h3></a>
			<?php endif; ?>	

			<?php if( !get_field('home_focus_link') ): ?>
			<h3><?php the_field('home_focus_call_to_action'); ?></h3>
			<?php endif; ?>	

	<?php endif; ?>	


</div><!-- ends home focus section -->

		<?php 
		$homeHeroImage = get_field('home_hero_image');
		if( !empty($homeHeroImage) ): ?>
		<img class="full-width-image" src="<?php echo $homeHeroImage['url']; ?>" alt="<?php echo $homeHeroImage['alt']; ?>" />
		<?php endif; ?>


		<div id="hero-logo-titles">
			<?php if( get_field('home_primary_title') ): ?>
				<div class="fade-in half">		
				<h1><?php the_field('home_primary_title'); ?><?php endif; ?></h1>
				<?php if( get_field('home_secondary_title') ): ?>
				<h2><?php the_field('home_secondary_title'); ?><?php endif; ?></h2>
				</div>
				<div class="fade-in full">	
				<?php if( get_field('home_attribution_title') ): ?>	
				<h3><?php the_field('home_attribution_title'); ?>
				<?php endif; ?></h3>
				</div><!-- ends fade in -->
		</div><!-- ends hero logo titles -->
	</div><!-- .entry-meta -->
<div id="content" class="site-content">
	<div id="primary" class="content-area">
	<main id="main" class="site-main" role="main">
		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		<header class="entry-header"> 	
		</header><!-- .entry-header -->
		<div class="entry-content">
			<!-- THIS IS FOR THE PAGE CONTENT INSERTED VIA THE EDITOR -->
				<?php while ( have_posts() ) : the_post(); ?>
				<?php the_content();?>
				<?php endwhile; // End of the loop. ?>
				<!--THIS IS FOR ALL POSTS IN EXCERPTED FORM -->
				<!-- SETTING UP THE QUERY -->
						<?php
						$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
						$args= array(
							'posts_per_page' => -1,
							'paged' => $paged
						);
						query_posts('cat=-151');
					?>


						<?php if(have_posts()) : ?>
						<?php while(have_posts()) : the_post(); ?>
						<!-- SETTING UP THE CONDITIONS -->
								<?php if (!empty($post->post_excerpt)) : ?>
								<!-- OUTPUTTING THE RESULT -->
								<!-- DRAWING IN EITHER THE THUMB OR FIRST IMAGE | FUNCTION INSERTED IN FUNCTIONS FILE -->		
								<div class="entry-content">
								<div class="archive-thumb"><a href="<?php the_permalink(); ?>" rel="bookmark"><img src="<?php echo my_image_display(); ?>"/></a></div> 
								<!-- AND NOW THE EXCERPT -->
								<div class="archive-excerpt"><h1 class="entry-title"><a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a></h1>
								<?php the_excerpt();?>
								<p class="archive-link"><a href="<?php the_permalink(); ?>" rel="bookmark">read more</a></p>
								</div><!-- ends archive excerpt -->
								</div>
								<?php else : ?>
								<h1 class="entry-title"><?php the_title(); ?></a></h1>
								<?php the_content(); ?>
								<?php endif; ?>
								<div class="clear"><hr/></div>
						<?php endwhile; ?>
						<?php endif; ?>
		</div><!-- .entry-content -->
			<footer class="entry-footer">		
			</footer><!-- .entry-footer -->
		</article><!-- #post-## -->
	</main><!-- #main -->
	</div><!-- #primary -->
<?php 
get_sidebar();
get_footer();
